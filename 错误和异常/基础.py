# -*- coding: utf-8 -*-
__author__ = 'luointo'

# a = 10 * (1 / 0)  # 0 不能作为除数，触发异常
# b = "1" + 3 # int 不能与 str 相加，触发异常

"""
异常处理
异常捕捉可以使用 try/except 语句
"""

# while True:
#     try:
#         x = int(input("请输入一个数字: "))
#     except ValueError:
#         print("您输入的不是数字，请再次尝试输入！")
#     else:
#         print("输入数字为:", x)
#     finally:
#         print("finally")


def test_func(a, b):
    try:
        print(a / b)
    except ZeroDivisionError:
        print('0不能做分母')
    except ValueError:
        print("类型错误")
    else:
        print('什么异常都没发生')


if __name__ == '__main__':
    test_func(10, 5)
    test_func(10, 0)
