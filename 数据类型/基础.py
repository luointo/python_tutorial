# -*- coding: utf-8 -*-
__author__ = 'luointo'


# 整数
"""
Python可以处理任意大小的整数，包括负整数，在程序中的表示方法和数学上的写法一模一样
"""
a1 = 12
# print(a1)

# 浮点数
b1 = 3.4
# print(b1)

# 字符串
"""
字符串是以单引号'或双引号"括起来的任意文本
"""
c1 = 'test data'
c2 = "abc"
c3 = """
python
"""
# print(c1)

# 布尔值
d1 = True
d2 = False

# 空值
"""
空值是Python里一个特殊的值，用None表示。None不能理解为0，因为0是有意义的，而None是一个特殊的空值
未初始化的意思
"""
e = None
print(e)
