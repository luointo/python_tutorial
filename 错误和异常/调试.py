# -*- coding: utf-8 -*-
__author__ = 'luointo'


def func():
    lst = []
    for x in range(10):
        if x % 2 == 0:
            lst.append(x)
    return lst


if __name__ == '__main__':
    result = func()
    print(result)
