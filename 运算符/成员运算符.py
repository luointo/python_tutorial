# -*- coding: utf-8 -*-
__author__ = 'luointo'

a = 10
b = 20
list_data = [1, 2, 3, 4, 5]

if a in list_data:
    print("1 - 变量 a 在给定的列表中 list_data 中")
else:
    print("1 - 变量 a 不在给定的列表中 list_data 中")

if b not in list_data:
    print("2 - 变量 b 不在给定的列表中 list_data 中")
else:
    print("2 - 变量 b 在给定的列表中 list_data 中")

# 修改变量 a 的值
a = 2
if a in list_data:
    print("3 - 变量 a 在给定的列表中 list_data 中")
else:
    print("3 - 变量 a 不在给定的列表中 list_data 中")